import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.*;

public class TestLogin {

    @BeforeAll
    public static void start() {
        out.println("Starting test...");
    }

    @AfterAll
    public static void end() {
        out.println("Ending test...");
    }

    // login
    @ParameterizedTest
    @CsvSource(value = {"anna, losen", "berit, 123456", "kalle, password"})
    void LoginSuccess(String username, String password) {
        Login login = new Login();
        String usernameAsBase64 = login.loginAuth(username, password);

        byte[] usernameBackAsBytes = Base64.getDecoder().decode(usernameAsBase64);
        String usernameBackAsString = new String(usernameBackAsBytes);

        assertEquals(username, usernameBackAsString);
        out.println("String username " + usernameBackAsString);
        out.println("Base64 username " + usernameAsBase64);

    }

    @ParameterizedTest
    @CsvSource(value = {"berit, losen, false"})
    void LoginUnsuccessful(String username, String password) {
        Login login = new Login();
        UnsupportedOperationException unsupportedOperationException = assertThrows(UnsupportedOperationException.class,
                () -> {
                    login.loginAuth(username, password);
                });

        assertEquals("Wrong password.",
                unsupportedOperationException.getMessage());
        //illegalargumentexception kolla upp
    }

    // auth
    @ParameterizedTest
    @ValueSource(strings = {"YW5uYQ==", "YmVyaXQ=", "a2FsbGU="})
    void tokenSuccess(String token) {
        Login login = new Login();
        boolean result = login.tokenAuth(token);

        assertTrue(result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"YW5uAnna", "YmVBerit", "a2FKalle"})
    void tokenUnsuccessful(String token) {
        Login login = new Login();
        boolean result = login.tokenAuth(token);

        assertFalse(result);
    }

    // token
    @ParameterizedTest
    @CsvSource(value = {
            "anna, losen, eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbm5hIiwiUm9sZSI6ImFkbWluIn0.p4byjHpGJwDqccXcCuMRxWvdKu-P_dS6HOX7XlxHLgc",
            "berit, 123456, eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJiZXJpdCIsIlJvbGUiOiJ0ZWFjaGVyIn0.jkNbZFgkQlNt8HTUeAriWuUNB9pa1Ehq81Te4QcB_yg",
            "kalle, password, eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrYWxsZSIsIlJvbGUiOiJzdHVkZW50In0.YBUI4trhEYMz8q-c4qlhP6lld9tF2y1GJyyHP3htn0c"
    })
    void jwtBuildSuccess(String username, String password, String expected) {
        Login login = new Login();
        String jwt = login.jwtBuild(username, password);

        assertEquals(expected, jwt);
    }

    @ParameterizedTest
    @CsvSource(value = {"wrong-token"})
    void checkTokenUnsuccess(String jwtToken) {
        Login login = new Login();
        UnsupportedEncodingException e = assertThrows(UnsupportedEncodingException.class,
                () -> {
                    login.checkJwt(jwtToken, "Grading");
                });

        assertEquals("Wrong JWT token.", e.getMessage());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbm5hIiwiUm9sZSI6ImFkbWluIn0.p4byjHpGJwDqccXcCuMRxWvdKu-P_dS6HOX7XlxHLgc, Grading"
    })
    void checkDomainWithToken(String token, String domain) throws UnsupportedEncodingException {
        Login login = new Login();
        ArrayList<String> jwt = (ArrayList<String>) login.checkJwt(token, domain);
        ArrayList<String> list = new ArrayList<>();

        out.println("Token: " + token);
        out.println("Domain: " + domain);
        list.add("Grading: read/write");
        out.println(list);
        
        assertEquals(list, jwt);
    }
}
