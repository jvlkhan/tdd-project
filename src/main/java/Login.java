import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static java.lang.System.out;

public class Login {

    public static HashMap<String, String> getUser() {
        HashMap<String, String> users = new HashMap<>();
        users.put("anna", "losen");
        users.put("berit", "123456");
        users.put("kalle", "password");
        return users;
    }

    public String loginAuth(String username, String password) {
        HashMap<String, String> users = getUser();
        if (Objects.equals(users.get(username), password)) {
            byte[] usernameAsBytes = username.getBytes();
            return Base64.getEncoder().encodeToString(usernameAsBytes);
        } else {
            throw new UnsupportedOperationException("Wrong password.");
        }
    }

    public boolean tokenAuth(String token) {
        byte[] decodedToken = Base64.getDecoder().decode(token);
        String usernameAsString = new String(decodedToken);

        return getUser().containsKey(usernameAsString);
    }

    String key = "super-duper-mega-secret-key";
    byte[] secretBytes = Base64.getEncoder().encode(key.getBytes());
    SecretKey secretKey = new SecretKeySpec(secretBytes, "HmacSHA256");

    public String jwtBuild(String username, String password) {
        HashMap<String, String> users = getUser();
        String role = "";

        if (Objects.equals(users.get(username), password)) {
            switch (username) {
                case "anna" -> role = "admin";
                case "berit" -> role = "teacher";
                case "kalle" -> role = "student";
                default -> throw new IllegalStateException("Unexpected value: " + username);
            }
        }

        return Jwts.builder()
                .setSubject(username)
                .claim("Role", role)
                .signWith(secretKey)
                .compact();
    }

    public List<String> checkJwt(String jwtToken, String domain) throws UnsupportedEncodingException {
        ArrayList<String> domainGrading = new ArrayList<>();
        ArrayList<String> domainFeedback = new ArrayList<>();

        String role;
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(secretKey)
                    .build()
                    .parseClaimsJws(jwtToken)
                    .getBody();
            role = claims.get("Role").toString();
            out.println(role);
        } catch (Exception e) {
            throw new UnsupportedEncodingException("Wrong JWT token.");
        }

        if ("admin".equals(role)) {
            domainGrading.add("Grading: read/write");
            domainFeedback.add("Course feedback: read/write");
        } else if ("teacher".equals(role)) {
            domainGrading.add("Grading: read/write");
            domainFeedback.add("Course feedback: read");
        } else if ("student".equals(role)) {
            domainGrading.add("Grading: read");
            domainFeedback.add("Course feedback: read/write");
        } else {
            throw new IllegalStateException("Unexpected value: " + role);
        }

        if ("Grading".equals(domain)) {
            return domainGrading;
        }
        if ("Course_feedback".equals(domain)) {
            return domainFeedback;
        }

        return domainGrading;
    }
}
